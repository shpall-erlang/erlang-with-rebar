FROM erlang:##ERL_VERSION## AS build-env

ARG rebar3_version=3.6.2

RUN apk add --update tar curl git bash make libc-dev gcc g++ vim && \
    rm -rf /var/cache/apk/*

RUN git clone https://github.com/erlang/rebar3.git && \
    cd rebar3 && \
    git checkout $rebar3_version && \
    ls -alch && \
    ./bootstrap && \
    ls -alch


FROM erlang:##ERL_VERSION##

RUN apk add --update --no-cache git

COPY --from=build-env /rebar3/rebar3 /usr/local/bin/
